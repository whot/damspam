# SPDX-Licences-Identifier: MIT

from damspam import (
    Payload,
    InvalidPayload,
    WebhookIssueEvent,
    DamSpamBuilder,
    DamSpamIssue,
    BuilderError,
)
from unittest.mock import patch, MagicMock
from datetime import datetime, timedelta, timezone

import attr
import json
import pytest


@pytest.fixture
def issue_event() -> dict:
    return json.load(open("data/issue-event.json"))


def test_payload():
    data = json.loads(
        """
    {
        "foo": "bar",
        "ls": [1, 2, 3],
        "obj": {
            "baz": 10,
            "bat": 11
        },
        "foos": [
            { "foa": 20 },
            { "fob": 21 }
        ]
    }
    """
    )

    payload = Payload.from_json(data)
    assert payload.get("foo").value == "bar"
    assert payload.get("ls").values == [1, 2, 3]
    assert payload.get("obj").get("baz").value == 10
    assert payload.get("obj").get("bat").value == 11
    assert payload.get("obj", "baz").value == 10
    assert payload.get("obj", "bat").value == 11
    assert payload.get("obj").values == {"baz": 10, "bat": 11}
    assert payload.get("foos").values == [{"foa": 20}, {"fob": 21}]
    assert payload.get("foos").values == [{"foa": 20}, {"fob": 21}]

    with pytest.raises(InvalidPayload):
        payload.get("invalid")

    with pytest.raises(InvalidPayload):
        payload = Payload.from_json({})


def test_webhook_abstractions(issue_event: dict):
    event = WebhookIssueEvent.from_json(issue_event)
    assert event.project.id == 1
    assert event.project.name == "Gitlab Test"
    assert event.project.namespace == "GitlabHQ"
    assert event.project.path_with_namespace == "gitlabhq/gitlab-test"
    assert event.project.web_url == "http://example.com/gitlabhq/gitlab-test"

    assert event.user.id == 1
    assert event.user.name == "Administrator"

    assert event.issue.id == 301
    assert event.issue.iid == 23
    assert event.issue.title == "New API: create/update/delete file"
    assert event.issue.author_id == 51

    assert event.added_labels[0].id == 205
    assert event.added_labels[0].title == "Platform"
    assert event.removed_labels[0].id == 206
    assert event.removed_labels[0].title == "API"


@patch("gitlab.Gitlab")
def test_builder(gitlab):
    @attr.s
    class Status:
        auth_called: bool = attr.ib(init=False, default=False)
        project: MagicMock = attr.ib(init=False, default=attr.Factory(MagicMock))  # type: ignore
        issue: MagicMock = attr.ib(init=False, default=attr.Factory(MagicMock))  # type: ignore
        tracker_issue: MagicMock = attr.ib(init=False, default=attr.Factory(MagicMock))  # type: ignore

    status = Status()

    def auth_called():
        status.auth_called = True

    gitlab.auth = auth_called
    builder = DamSpamBuilder.create_from_instance(gitlab)
    assert not status.auth_called, "Creating from instance must not call auth()"

    def projects_get(**kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == "test/foo"
        return status.project

    gitlab.projects = MagicMock()
    gitlab.projects.get = projects_get
    builder.set_project("test/foo")

    # Our API doesn't care if you call set_project twice, let's make use of that here

    def projects_get_id(**kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == 457
        return status.project

    gitlab.projects = MagicMock()
    gitlab.projects.get = projects_get_id
    builder.set_project_id(457)
    assert builder.project == status.project

    def issues_get(*args, **kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == 123
        return status.issue

    status.project.issues = MagicMock()
    status.project.issues.get = issues_get

    builder.set_issue_iid(123)
    assert builder.issue == status.issue

    builder.set_readonly(True)
    assert builder.readonly is True

    builder.set_readonly(False)
    assert builder.readonly is False

    def projects_get_tracker(**kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == "tracker/project"
        return status.project

    def issues_get_tracker(*args, **kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == 99
        return status.tracker_issue

    gitlab.projects.get = projects_get_tracker
    status.project.issues.get = issues_get_tracker
    builder.set_tracker_project_issue("tracker", "project", 99)
    assert builder.tracker_issue == status.tracker_issue

    # tracker == issue check
    with pytest.raises(BuilderError):
        builder.set_tracker_issue(status.issue)

    damspam = builder.build_damspam_issue()
    assert damspam.gl == gitlab
    assert damspam.project == status.project
    assert damspam.issue == status.issue
    assert damspam.tracker_issue == status.tracker_issue
    assert damspam.readonly is False


def test_link_to_tracker():
    gl, project, issue, tracker_issue = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    issue.id = 123
    issue.iid = 567
    issue.title = "some test issue"
    project.name = "pytest run"
    tracker_issue.title = "tracker issue title"
    tracker_issue.project_id = 1
    tracker_issue.iid = 99

    links = []

    issue.links = MagicMock()
    issue.links.create = lambda args: links.append(args)

    damspam = DamSpamIssue(gl, project, issue, tracker_issue)
    # expect nothing to happen
    damspam.readonly = True
    damspam.link_to_tracker()
    assert links == []

    # epxect the links to be called with the tracker issue data
    damspam.readonly = False
    damspam.link_to_tracker()
    assert links == [
        {
            "target_project_id": tracker_issue.project_id,
            "target_issue_iid": tracker_issue.iid,
        }
    ]

    # expect nothing to happen if we don't have a tracker issue
    links = []
    damspam.tracker_issue = None
    damspam.readonly = True
    damspam.link_to_tracker()
    assert links == []

    damspam.tracker_issue = issue
    with pytest.raises(AssertionError):
        damspam.link_to_tracker()


def test_mark_as_spam():
    gl, project, issue, tracker_issue = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    issue.id = 123
    issue.iid = 567
    issue.title = "some test issue"
    project.name = "pytest run"
    tracker_issue.title = "tracker issue title"
    tracker_issue.project_id = 1
    tracker_issue.iid = 99

    @attr.s
    class Status:
        saved: bool = attr.ib(default=False)

    status = Status()

    def issue_save(*args, **kwargs):
        assert not args
        assert not kwargs
        status.saved = True

    issue.labels = ["existing-label"]
    issue.confidential = False
    issue.save = issue_save

    damspam = DamSpamIssue(gl, project, issue, tracker_issue)
    # expect nothing to happen
    damspam.readonly = True
    damspam.mark_as_spam()
    assert issue.labels == ["existing-label"]
    assert issue.confidential is False
    assert status.saved is False

    # now for realz
    damspam.readonly = False
    damspam.mark_as_spam()
    assert sorted(issue.labels) == sorted(["existing-label", "spam"])
    assert issue.confidential is True
    assert status.saved is True

    damspam.tracker_issue = issue
    with pytest.raises(AssertionError):
        damspam.mark_as_spam()


def test_block_issue_creator():
    gl, project, issue, tracker_issue = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    issue.id = 123
    issue.iid = 567
    issue.title = "some test issue"
    issue.author = {
        "id": 987,
        "state": "active",
    }
    project.name = "pytest run"
    tracker_issue.title = "tracker issue title"
    tracker_issue.project_id = 1
    tracker_issue.iid = 99

    @attr.s
    class Status:
        blocked: bool = attr.ib(default=False)

    status = Status()

    def user_block(*args):
        assert not args
        status.blocked = True

    spammer = MagicMock()
    spammer.created_at = datetime.isoformat(datetime.now(tz=timezone.utc))
    spammer.block = user_block

    def users_get(*args):
        assert args[0] == 987
        return spammer

    gl.users = MagicMock()
    gl.users.get = users_get

    damspam = DamSpamIssue(gl, project, issue, tracker_issue)
    damspam.readonly = True
    damspam.block_issue_creator()
    assert status.blocked is False

    damspam.readonly = False
    damspam.block_issue_creator()
    assert status.blocked is True

    # check the 6 months time barrier
    status.blocked = False
    dt = timedelta(weeks=26)
    spammer.created_at = datetime.isoformat(datetime.now(tz=timezone.utc) - dt)
    damspam.readonly = False
    damspam.block_issue_creator()
    assert status.blocked is False
