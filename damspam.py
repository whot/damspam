#!/usr/bin/env python3
#
# SPDX-Licences-Identifier: MIT

import damspam.cli

if __name__ == "__main__":
    damspam.cli.damspam()
