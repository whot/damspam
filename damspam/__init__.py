# SPDX-Licences-Identifier: MIT

__version__ = "0.1.0"

from typing import Any, Optional
from datetime import datetime, timedelta, timezone

import attr
import gitlab
import gitlab.v4.objects
import logging

logging.basicConfig(format="%(levelname)7s| %(name)s: %(message)s")
logger = logging.getLogger("damspam")


@attr.s
class Payload:
    """
    Wrapper class to more conveniently access a json dictionary. In particular, this
    throws the InvalidPayload exception when trying to access a field that doesn't exist.

    This is not a complete wrapper, it has just enough functionality to let us
    parse the GitLab webhook payload for the fields we care bout.
    """

    payload: dict = attr.ib()

    @property
    def value(self):
        """
        Return the value of this item. If the value is a list or a dict, the value is
        dynamically wrapped into a new Payload so we can call `get()` and `value` on
        it again.

        If you need to access the list/dict items directly, use `values`
        """
        # Dynamically wrap any lists/dict values into new payload items
        if isinstance(self.payload, list):
            return [Payload(item) for item in self.payload]
        if isinstance(self.payload, dict):
            return {k: Payload(v) for k, v in self.payload}
        return self.payload

    @property
    def values(self):
        """
        Return the value of this item as-is if it is a list or a dict.
        """
        if isinstance(self.payload, list) or isinstance(self.payload, dict):
            return self.payload
        raise ValueError("Not an iterable value")

    def get(self, key, *keys) -> Any:
        """
        Get the data for the given key from the payload, stepping down into
        the extra keys if any are given::

        >>> import json
        >>> data = '{"foo": {"bar": "bat"}}'
        >>> payload = Payload.from_json(json.loads(data))
        >>> payload.get("foo").values
        {"bar", "bat"}
        >>> payload.get("foo", "bar").value
        bat

        """
        try:
            data = self.payload[key]
            for k in keys:
                data = data[k]
            return Payload(data)
        except KeyError as e:
            raise InvalidPayload(
                f"Missing key(s): '{e.args[0]}' ({'→'.join([key, *keys])}) in {self.payload}"
            )

    @classmethod
    def from_json(cls, json: dict) -> "Payload":
        if not json:
            raise InvalidPayload("Empty payloads are not allowed")
        return cls(payload=json)

    def __str__(self):
        return str(self.payload)


@attr.s
class InvalidPayload(Exception):
    """Thrown when trying to access a non-existing field in a payload"""

    message: str = attr.ib()


@attr.s
class WebhookGitlabUser:
    id: int = attr.ib()
    name: str = attr.ib()

    @classmethod
    def from_json(cls, payload: Payload) -> "WebhookGitlabUser":
        return cls(**{key: payload.get(key).value for key in ["id", "name"]})


@attr.s
class WebhookGitlabIssue:
    id: int = attr.ib()
    iid: int = attr.ib()
    title: str = attr.ib()
    author_id: int = attr.ib()

    @classmethod
    def from_json(cls, payload: Payload) -> "WebhookGitlabIssue":
        return cls(
            **{
                key: payload.get(key).value
                for key in ["id", "iid", "title", "author_id"]
            }
        )


@attr.s
class WebhookGitlabProject:
    id: int = attr.ib()
    name: str = attr.ib()
    namespace: str = attr.ib()
    path_with_namespace: str = attr.ib()
    web_url: str = attr.ib()

    @classmethod
    def from_json(cls, payload: Payload) -> "WebhookGitlabProject":
        return cls(
            **{
                key: payload.get(key).value
                for key in ["id", "name", "namespace", "path_with_namespace", "web_url"]
            }
        )


@attr.s
class WebhookGitlabLabel:
    id: int = attr.ib()
    title: str = attr.ib()

    @classmethod
    def from_json(cls, payload: Payload) -> "WebhookGitlabLabel":
        return cls(**{key: payload.get(key).value for key in ["id", "title"]})


@attr.s
class WebhookIssueEvent:
    project: WebhookGitlabProject = attr.ib()
    user: WebhookGitlabUser = attr.ib()
    issue: WebhookGitlabIssue = attr.ib()
    labels: list[WebhookGitlabLabel] = attr.ib()
    added_labels: list[WebhookGitlabLabel] = attr.ib()
    removed_labels: list[WebhookGitlabLabel] = attr.ib()

    @classmethod
    def from_json(cls, json: dict) -> "WebhookIssueEvent":
        payload = Payload.from_json(json)
        project = WebhookGitlabProject.from_json(payload.get("project"))
        user = WebhookGitlabUser.from_json(payload.get("user"))
        issue = WebhookGitlabIssue.from_json(payload.get("object_attributes"))
        labels = [WebhookGitlabLabel.from_json(l) for l in payload.get("labels").value]
        # Webhook test payloads have an empty "changes" field - if that
        # happens use the labels from the issue and treat every label
        # as newly added
        try:
            payload.get("changes", "labels")
        except InvalidPayload:
            logger.warning("No changed labels in data, using current label set instead")
            labels_before = []
            labels_current = labels
        else:
            labels_before = [
                WebhookGitlabLabel.from_json(l)
                for l in payload.get("changes", "labels", "previous").value
            ]
            labels_current = [
                WebhookGitlabLabel.from_json(l)
                for l in payload.get("changes", "labels", "current").value
            ]

        removed_labels = list(filter(lambda e: e not in labels_current, labels_before))
        added_labels = list(filter(lambda e: e not in labels_before, labels_current))

        return cls(
            project=project,
            user=user,
            issue=issue,
            labels=labels,
            added_labels=added_labels,
            removed_labels=removed_labels,
        )


@attr.s
class BuilderError(Exception):
    message: str = attr.ib()

    def __str__(self):
        return f"Builder Error: {self.message}"


@attr.s
class DamSpamBuilder:
    """
    Builder class for DamSpam. Use this to set the fields required for
    a specific
    """

    gl: gitlab.Gitlab = attr.ib()  # type: ignore
    project: Optional[gitlab.v4.objects.Project] = attr.ib(default=None)
    issue: Optional[gitlab.v4.objects.ProjectIssue] = attr.ib(init=False, default=None)
    tracker_issue: Optional[gitlab.v4.objects.ProjectIssue] = attr.ib(
        init=False, default=None
    )
    # We default to readonly true so a runaway bot can't change anything and any copies of
    # this object explicitly needs to disable readonly
    readonly: bool = attr.ib(init=False, default=True)

    @classmethod
    def create_from_url(cls, url: str, private_token: str) -> "DamSpamBuilder":
        logger.debug(f"Using GitLab instance at: {url}")
        gl = gitlab.Gitlab(url=url, private_token=private_token)  # type: ignore
        gl.auth()
        return cls(gl=gl)

    @classmethod
    def create_from_instance(
        cls,
        gl: gitlab.Gitlab,  # type: ignore
    ) -> "DamSpamBuilder":
        return cls(gl=gl)

    @classmethod
    def create_from_project(
        cls, gl: gitlab.Gitlab, project: gitlab.v4.objects.Project  # type: ignore
    ) -> "DamSpamBuilder":
        return cls(gl=gl, project=project)

    def set_project(self, full_name: str) -> "DamSpamBuilder":
        self.project = self.gl.projects.get(id=full_name)
        if self.project is None:
            raise BuilderError(f"Project {full_name} not found")
        logger.debug(f"Using GitLab project: {full_name}")
        return self

    def set_project_id(self, id: int) -> "DamSpamBuilder":
        self.project = self.gl.projects.get(id=id)
        if self.project is None:
            raise BuilderError("Project {id} not found")
        logger.debug(f"Using GitLab project: {self.project.path_with_namespace}")
        return self

    def set_issue_iid(self, issue_iid: int) -> "DamSpamBuilder":
        logger.debug(f"Using GitLab project issue: #{issue_iid}")
        if self.project is None:
            raise BuilderError("Cannot fetch an issue without a project")
        issue = self.project.issues.get(id=issue_iid)
        if issue is None:
            raise BuilderError("Project issue {issue_iid} not found")
        if issue == self.tracker_issue:
            raise BuilderError("Tracker issue must differ from issue")
        self.issue = issue
        return self

    def set_readonly(self, readonly: bool) -> "DamSpamBuilder":
        self.readonly = readonly
        return self

    def set_tracker_project_issue(
        self, namespace: str, project: str, iid: int
    ) -> "DamSpamBuilder":
        tracker_project = self.gl.projects.get(id=f"{namespace}/{project}")
        if tracker_project is None:
            raise BuilderError("Tracker project '{namespace}/{project}' not found")
        tracker_issue = tracker_project.issues.get(id=iid)
        if tracker_issue is None:
            raise BuilderError("Tracker issue '{namespace}/{project}#{iid}' not found")

        if self.issue == tracker_issue:
            raise BuilderError("Tracker issue must differ from issue")

        self.tracker_issue = tracker_issue

        logger.debug(f"Using tracker issue: {namespace}/{project}#{iid}")
        return self

    def set_tracker_issue(
        self, tracker_issue: gitlab.v4.objects.ProjectIssue
    ) -> "DamSpamBuilder":
        if self.issue == tracker_issue:
            raise BuilderError("Tracker issue must differ from issue")
        self.tracker_issue = tracker_issue
        return self

    def build_damspam_issue(self) -> "DamSpamIssue":
        if any([x is None for x in (self.project, self.issue, self.gl)]):
            raise BuilderError("Cannot build without all fields set")
        # to shut up mypy:
        assert self.gl is not None
        assert self.project is not None
        assert self.issue is not None
        return DamSpamIssue(
            gl=self.gl,
            project=self.project,
            issue=self.issue,
            readonly=self.readonly,
            tracker_issue=self.tracker_issue,
        )


@attr.s
class DamSpamIssue:
    gl: gitlab.Gitlab = attr.ib()  # type: ignore
    project: gitlab.v4.objects.Project = attr.ib()
    issue: gitlab.v4.objects.ProjectIssue = attr.ib()
    tracker_issue: Optional[gitlab.v4.objects.ProjectIssue] = attr.ib(default=None)
    readonly: bool = attr.ib(default=False)

    @property
    def ro_prefix(self) -> str:
        return "[RO SKIP] " if self.readonly else ""

    def block_issue_creator(
        self,
        recursive: bool = True,
    ) -> None:
        """
        Block the user that created the issue in this DamSpam instance. If
        recursive is True, all issues opened by them are tagged as "spam" and made
        confidential.
        """
        if self.issue.author["state"] != "active":
            logger.info(
                f"Author '{self.issue.author['name']}' is already {self.issue.author['state']}, nothing to do"
            )
            return

        # Safety check: if we have more than 2 commenters on the issue, we
        # say we don't know what to do.
        commenters = {n.author["id"] for n in self.issue.notes.list()}
        if len(commenters) > 2:
            logger.error("Three or more people involved, please report directly")
            return

        # Block the user first, ideally that way they don't get notified about the rest of the
        # work we do here.
        spammer = self.gl.users.get(self.issue.author["id"])
        if not spammer:
            logger.error("Failed to look up spammer {self.issue.author['id']} by id")
            return

        # Safety check: if the profile is more than 6 months old, don't mark them
        # as spammer. This is mostly to save us from accidentally marking a long-term
        # contributor as spammer and then having to manually undo hundreds of issues.
        try:
            created_at = datetime.fromisoformat(spammer.created_at)
            now = datetime.now(tz=timezone.utc)
            dt = timedelta(weeks=26)
            if created_at < now - dt:
                logger.error(
                    f"Account of alleged spammer {spammer.name} is more than 6m old, skipping"
                )
                return
        except (ValueError, TypeError) as e:
            logger.error(f"Unable to parse created_at from spammer: {e}")

        logger.info(f"{self.ro_prefix}Blocking user '{spammer.name}'")
        if not self.readonly:
            spammer.block()

        self.mark_as_spam()
        self.link_to_tracker()

        if recursive:
            # find other issues by this author, mark them all as spam.
            # Python gitlab has Issues and ProjectIssues, only the latter can be edited.
            issues_to_block = self.gl.issues.list(
                author_id=self.issue.author["id"],
                scope="all",
                iterator=True,  # handles pagination for us
            )
            for issue in filter(lambda i: i.id != self.issue.id, issues_to_block):
                if issue.project_id != self.project.id:
                    project = self.gl.projects.get(issue.project_id)
                    if project is None:
                        logger.warning(
                            "Unable to access project {issue.project_id} for recursive block"
                        )
                        continue
                    builder = DamSpamBuilder.create_from_project(self.gl, project)
                else:
                    builder = DamSpamBuilder.create_from_project(self.gl, self.project)

                builder.set_issue_iid(issue.iid).set_readonly(self.readonly)

                try:
                    if self.tracker_issue:
                        builder.set_tracker_issue(self.tracker_issue)
                except BuilderError:
                    pass  # Issue == tracker_issue, can't have that
                else:
                    damspam = builder.build_damspam_issue()
                    damspam.mark_as_spam()
                    damspam.link_to_tracker()

    def mark_as_spam(self):
        """
        Labels a single issue as spam and marks it confidential
        """
        assert self.tracker_issue != self.issue

        logger.info(
            f"{self.ro_prefix}Processing spam issue {self.issue.id} ({self.project.name}#{self.issue.iid}): {self.issue.title}"
        )
        if not self.readonly:
            labels = self.issue.labels
            self.issue.labels = ["spam"] + labels
            self.issue.confidential = True
            self.issue.save()

    def link_to_tracker(self):
        """
        Links this issue to the spam tracker issue
        """
        if not self.tracker_issue:
            logger.warning("No tracker issue set, cannot link")
            return

        assert self.tracker_issue != self.issue

        logger.info(
            f"{self.ro_prefix}Linking to '{self.tracker_issue.title}': {self.issue.id} ({self.project.name}#{self.issue.iid}): {self.issue.title}"
        )
        if not self.readonly:
            data = {
                "target_project_id": self.tracker_issue.project_id,
                "target_issue_iid": self.tracker_issue.iid,
            }
            self.issue.links.create(data)
