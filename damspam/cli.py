# SPDX-Licences-Identifier: MIT
#
# This file is formatted with Python Black

from typing import Optional, Tuple

import attr
import click
import logging
import json
import os

import damspam as ds

logger = logging.getLogger("damspam")


@attr.s
class Config:
    verbose: bool = attr.ib()
    readonly: bool = attr.ib()
    token: Optional[str] = attr.ib()


class GitlabIssueSpec(click.ParamType):
    name = "GitLab issue <namespace/name#iid>"

    def convert(self, value, param, ctx):
        try:
            path, issue = value.split("#")
            ns, name = path.split("/")
            iid = int(issue, 10)
            return (ns, name, iid)
        except ValueError:
            self.fail(f"{value!r} is not a valid issue spec", param, ctx)


@click.group()
@click.option("--verbose", "-v", is_flag=True, help="Enable debug logging")
@click.option("--readonly", is_flag=True, help="Print, but don't modify")
@click.option(
    "--token-file",
    type=click.Path(exists=True, dir_okay=False),
    help="File containing the GitLab API token",
)
@click.option(
    "--token-env",
    type=str,
    help="Environment variable containing the GitLab API token",
)
@click.pass_context
def damspam(
    ctx,
    verbose: bool,
    readonly: bool,
    token_file: Optional[click.Path],
    token_env: Optional[str],
):
    global logger
    """
    Identify a spammer and pseudo-remove them from gitlab. Currently this works by
    taking an issue, then blocking the creater of that issue and hiding all issues
    filed by the same user.

    This tool does *not* permanently ban the user, doing so requires manual effort
    by a GitLab administrator. Instead, by blocking the user and hiding all issues
    it intends to make GitLab spam less useful.

    This tool is intended to be passed a GitLab issue-event webhook payload, see
    'damspam issue-hook --help'. It can be run manually with the 'issue' command.
    The functionality is largely identical.
    """
    logger.setLevel(logging.DEBUG if verbose else logging.INFO)

    if token_file is not None:
        token = open(token_file).read().rstrip()  # type: ignore
    elif token_env is not None:
        try:
            token = os.environ[token_env]
        except KeyError:
            click.secho(
                "Requested token environment variable does not exist, use --token-env"
            )
            raise SystemExit(1)
    else:
        click.secho("One of --token-file or --token-env is required")
        raise SystemExit(1)

    ctx.obj = Config(verbose=verbose, readonly=readonly, token=token)


@damspam.command(name="hook-hide-issue", help="Process a GitLab issue event webhook")
@click.option(
    "--payload-file",
    type=click.Path(exists=True, dir_okay=False),
    help="File containing the payload",
)
@click.option(
    "--payload-env", type=str, help="Environment variable containing the payload"
)
@click.option(
    "--label",
    type=str,
    default="spam",
    help="The newly created label to look for (default: spam)",
)
@click.option(
    "--recursive/--no-recursive",
    default=True,
    help="Recursively hide all issues by the user that created this issue",
)
@click.option(
    "--tracker-issue",
    type=GitlabIssueSpec(),
    help="The spam tracker issue to link this issue with",
    default="freedesktop/freedesktop#548",
)
@click.option(
    "--skip-tracker-issue",
    type=bool,
    is_flag=True,
    default=False,
    help="Skip linking to the tracker issue",
)
@click.pass_context
def do_hide_issue_webhook(
    ctx,
    payload_file: click.Path,
    payload_env: str,
    label: str,
    recursive: bool,
    tracker_issue: Tuple[str, str, int],
    skip_tracker_issue: bool,
):
    """
    Process the payload from a Gitlab issue-event web hook. This hook checks for
    whether the "spam" label has been added and if so, it proceeds to look up the
    user that filed this issue and proceeds to block that user and
    hides all issues opened by that user.
    """
    if payload_file is not None:
        payload_string = open(payload_file).read()  # type: ignore
    else:
        try:
            payload_string = os.environ[payload_env]
        except KeyError:
            click.secho(
                "Requested payload environment variable does not exist, use --payload-from"
            )
            raise SystemExit(1)

    try:
        js = json.loads(payload_string)
        event = ds.WebhookIssueEvent.from_json(js)
        logger.debug(
            f"Webhook issue event for {event.project.path_with_namespace}#{event.issue.iid}"
        )
    except json.JSONDecodeError as e:
        click.secho(f"JSON parser failed: {e}")
        raise SystemExit(1)
    except ds.InvalidPayload as e:
        click.secho(f"Invalid data: {e}")
        raise SystemExit(1)

    # We have the webhook data parsed as event now, let's do our thing

    if label not in [l.title for l in event.added_labels]:
        logger.debug(f"No new label {label}")
        raise SystemExit(0)

    # FIXME: we need to filter our own bot so we don't react to our own events

    # project URL includes the project path
    instance_url = event.project.web_url.removesuffix(event.project.path_with_namespace)

    config = ctx.obj
    builder = (
        ds.DamSpamBuilder.create_from_url(instance_url, config.token)
        .set_project_id(event.project.id)
        .set_issue_iid(event.issue.iid)
        .set_readonly(config.readonly)
    )

    if not skip_tracker_issue:
        (ns, name, iid) = tracker_issue
        builder = builder.set_tracker_project_issue(ns, name, iid)

    dam = builder.build_damspam_issue()
    dam.block_issue_creator(recursive=recursive)


@damspam.command(name="hide-issue", help="Process the given GitLab spam issue")
@click.option(
    "--instance",
    type=str,
    default="https://gitlab.freedesktop.org",
    help="The GitLab instance",
)
@click.option(
    "--recursive/--no-recursive",
    default=True,
    help="Recursively hide all issues by the user that created this issue",
)
@click.option(
    "--tracker-issue",
    type=GitlabIssueSpec(),
    help="The spam tracker issue to link this issue with",
    default="freedesktop/freedesktop#548",
)
@click.option(
    "--skip-tracker-issue",
    type=bool,
    is_flag=True,
    help="Skip linking to the tracker issue",
)
@click.argument(
    "project",
    type=str,
)
@click.argument(
    "issue",
    type=int,
)
@click.pass_context
def do_hide_issue(
    ctx,
    instance: str,
    recursive: bool,
    tracker_issue: Tuple[str, str, int],
    skip_tracker_issue: bool,
    project: str,
    issue: int,
):

    """
    Hide the given issue and block the creating user as well as (if operating recursively)
    hiding all issues created by the same user.
    """
    config = ctx.obj
    builder = (
        ds.DamSpamBuilder.create_from_url(instance, config.token)
        .set_project(project)
        .set_issue_iid(issue)
        .set_readonly(config.readonly)
    )

    if not skip_tracker_issue:
        (ns, name, iid) = tracker_issue
        builder = builder.set_tracker_project_issue(ns, name, iid)
    dam = builder.build_damspam_issue()
    dam.block_issue_creator(recursive=recursive)


if __name__ == "__main__":
    damspam()
