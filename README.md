# damspam

A bot to dam the influx of GitLab spam. This bot is primarily invoked from
other bots

## Usage

As always, `damspam --help` lists the commands available. Commands are grouped into
`foo-bar` and `foo-bar-hook`. The two commands are usually the same, but the `-hook`
variant is designed to work with the payload from a GitLab Webhook.

To trigger `damspam` for a fake GitLab Webhook for an issue event, use:

```
damspam hide-issue-hook --payload-from path/to/payload-file
damspam hide-issue-hook --payload-env PAYLOAD_VAR
```

## Tracker issues

When hiding spam issues, the bot will link the hidden issue with a tracker
issue. The intention here is that while the issues are hidden (confidential),
someone still needs to be able to see them to work through and purge them.

This is easiest by linking each spam issue into one central issue - unlinking
could eventually trigger another bot to undo the work of the spam label.

This issue is by default
[freedesktop/freedesktop#548](https://gitlab.freedesktop.org/freedesktop/freedesktop/-/issues/548)
But can be disabled with the `--no-tracker-issue` argument.

For testing, it may be overridden with the `--tracker-issue` argument uses that
issue.


## Testing locally

Create an API token and store its content in `/tmp/token.txt`. This is your `--token-file`.
To test the CLI for a specific issue, in this case [`whot/damspam#2`](https://gitlab.freedesktop.org/whot/damspam/-/issues/2):

You *really* should use `--readonly` to avoid making any actual changes. All
actions that the bot would but doesn't do are prefixed with `[RO SKIP]` in the
logs.

```
$ ./damspam.py \
   --readonly \
   --verbose \
   --token-file=/tmp/token.txt
   hide-issue
   whot/damspam 2
```

The above runs, *in readonly* mode the `hide-issue` hook. To do the same but based on
a Webhook payload, save the issue event payload data (see below) in the file
`/tmp/issue-event-payload`:

```
# Note: this only works for if the "spam" label is set
$ ./damspam.py \
   --readonly \
   --verbose \
   --token-file=/tmp/token.txt
   hide-issue-hook
   --payload-file=/tmp/issue-event-payload.json
```

To get the payload data, go the the project Settings in GitLab, add a fake
Webhook for `http://example.com`, then click `Test`, `Issues Events` and
finally `View Details`. Note that for webhook tests, the `changes` JSON field is empty.
